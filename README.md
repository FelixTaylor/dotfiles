# :floppy_disk: dotfiles

This are my dotfiles for Linux Mint.

**Important**: All scripts in this repository are highly customized and may cause
damage on your system. So **DO NOT** run any of these scripts if you do not
fully understand the consequences.


### Software

*(Work in progress)*

| Software                                            | In Use | Dotfiles | Alternatives                                                                            |
| --------------------------------------------------- | ------ | -------- | --------------------------------------------------------------------------------------- |
| [oh-my-zsh](https://github.com/ohmyzsh/ohmyzsh)     | ✅     | ✅       |                                                                                         |
| [OpenBox](http://openbox.org/wiki/Main_Page)        | ✅     |          |                                                                                         |
| [MenuMaker](http://menumaker.sourceforge.net/)      |        |          |                                                                                         |
| [pandoc](https://pandoc.org/)                       | ✅     |          |                                                                                         |
| [peek](https://github.com/phw/peek)                 | ✅     |          | [LICEcap](https://licecap.en.softonic.com/), [rofi](https://github.com/davatorium/rofi) |
| [ripgrep](https://github.com/BurntSushi/ripgrep)    | ✅     |          | [grep](https://www.man7.org/linux/man-pages/man1/grep.1.html)                           |
| [tint2](https://wiki.archlinux.org/index.php/Tint2) |        |          |                                                                                         |
| [tmux](https://github.com/tmux/tmux)                | ✅     | ✅       |                                                                                         |
| [uLauncher](https://github.com/Ulauncher/Ulauncher) | ✅     |          |                                                                                         |
| [neovim](https://neovim.io/)                        | ✅     | ✅       | [atom](https://atom.io/), [vsc](https://code.visualstudio.com/)                         |


### plugins

#### vim
[Auto-Pairs](https://github.com/jiangmiao/auto-pairs), [TPM](https://github.com/tmux-plugins/tpm),
[any-jump](https://github.com/pechorin/any-jump.vim), [tmux-continuum](https://github.com/tmux-plugins/tmux-continuum),
[CtrlP](https://github.com/ctrlpvim/ctrlp.vim), [gitgutter](https://github.com/airblade/vim-gitgutter),
[gruvbox](https://github.com/morhetz/gruvbox), [limelight](https://github.com/junegunn/limelight.vim),
[NerdCommenter](https://github.com/scrooloose/nerdcommenter), [NerdTree](https://github.com/scrooloose/nerdtree),
[neoclide](https://github.com/neoclide/coc.nvim), [obsession](https://github.com/tpope/vim-obsession),
[terminus](https://github.com/wincent/terminus), [undotree](https://github.com/mbbill/undotree),
[vimwiki](https://github.com/vimwiki/vimwiki), [vim-obsession](https:github.com/pechorin/any-jump.vim),
[vim-airline](https://github.com/vim-airline/vim-airline), [vim-pandoc-syntax](https://github.com/vim-pandoc/vim-pandoc-syntax),
[vim-tmux-navigator](https://github.com/christoomey/vim-tmux-navigator),
[vim-tmux-resizer](https://github.com/RyanMillerC/better-vim-tmux-resizer)

#### tmux
[TPM](https://github.com/tmux-plugins/tpm), [tmux-continuum](https://github.com/tmux-plugins/tmux-continuum),
[tmux-pain-control](https://github.com/tmux-plugins/tmux-pain-control), [better-vim-tmux-resizer](https://github.com/RyanMillerC/better-vim-tmux-resizer),
[vim-tmux-navigator](https://github.com/christoomey/vim-tmux-navigator)

## Installation

In order for the install.sh script to work the repository has to be placed
under the `~/Documents/Git/dotfiles` directory.

```bash
$ git clone https://github.com/FelixTaylor/dotfiles.git
$ cd dotfiles
$ bash install.sh
```

------------------------------------------------------------------------------

_Last updated 2021-05-02_
