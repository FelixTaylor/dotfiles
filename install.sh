#!/bin/bash

print_usage() {
    RED='\033[0;31m'
    NC='\033[0m'
    printf "
    version:      5.0.4
    since:        2019-05-22
    last updated: 2023-02-22
    repository:   www.gitlab.com/FelixTaylor/dotfiles
    author:       Felix Taylor
    licence:      GPLv3

${RED}DESCRIPTION${NC}
    This is a setup script used to install software I use on a daily base on an
    new UNIX system. It originated from the setup.sh (v3.0.5) script. This
    script will override any existing dotfile including .zshrc, .tmux.conf,
    .vimrc, .terminfo files and .aliases. Do NOT run this script if you do not
    fully understand the consequences.

${RED}ERROR-CODES:${NC}
    ${RED}1${NC}: The script will exit with error code 1 if the operating system
    is not 'Linux'.

    ${RED}2${NC}: The script will exit with error code 2 if the user did not
    except the promt with Y/y in order to continue the script.

${RED}OPTIONS${NC}
    ${RED}-y${NC}
    Bypasses user validation and will run this script. DO NOT USE this option
    unless you fully understand the consiquences by running this script. I will
    take no responsibility for any damage done by this script."
}

is_not_linux() {
    [[ $(uname -s) != "Linux" ]]
}

# ------------------------------------------------------------------------------

accept='false'

while getopts 'y' flag; do
    case "${flag}" in
        y) accept='true' ;;
        *) print_usage
            exit ;;
    esac
done

if [[ "$accept" == "false" ]]; then
    read -p "This is my setup script which will create symbolic links to my .tmux.conf, .vimrc,
.zshrc, init.vim, .aliases and .terminfo to the current home directory. The
script will move any previously existing dotfile to the ~/old_dotfiles folder
and install needed software.

    ** I will take no responsibility for any damage done by this script. **

Only continue if you fully understand the consequences!
Do you want to continue? (Y/n): " -r
    [[ $REPLY =~ ^[Yy]$  ]] || exit 2
fi

if is_not_linux; then
    echo "Your OS is not supported by this script."
    exit 1
fi

sudo -v

source scripts/bash/install_base.sh

sudo systemctl enable cron
/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/JetBrains/JetBrainsMono/master/install_manual.sh)"
git clone https://github.com/tmux-plugins/tpm.git ~/.tmux/plugins/tpm

source scripts/bash/install_peek.sh
source scripts/bash/install_u_launcher.sh
#source scripts/bash/install_nordvpn.sh
#source scripts/bash/install_docker.sh
source scripts/bash/install_rust.sh
source scripts/bash/install_oh_my_zsh.sh
source scripts/bash/install_terminfo.sh
source scripts/bash/install_dotfiles.sh

cargo install zoxide --locked
sudo apt-get clean
sudo apt autoremove

echo -e "\nWow! Such power, much user 💖 🎉"

