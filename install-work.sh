#!/bin/bash

print_usage() {
    RED='\033[0;31m'
    NC='\033[0m'
    printf "
    version:      1.0.0
    since:        2021-11-11
    last updated: 2021-11-11
    repository:   github.com/FelixTaylor/dotfiles
    author:       Felix Taylor
    licence:      GPLv3

${RED}DESCRIPTION${NC}
    This is a setup script used to install software I use on a daily base on an
    new UNIX system. It originated from the setup.sh (v3.0.5) script. This
    script will override any existing dotfile including .zshrc, .tmux.conf,
    .vimrc, .terminfo files and .aliases. Do NOT run this script if you do not
    fully understand the consequences.

${RED}ERROR-CODES:${NC}
    ${RED}1${NC}: The script will exit with error code 1 if the operating system
    is not 'Linux'.

    ${RED}2${NC}: The script will exit with error code 2 if the user did not
    except the promt with Y/y in order to continue the script.

${RED}OPTIONS${NC}
    ${RED}-y${NC}
    Bypasses user validation and will run this script. DO NOT USE this option
    unless you fully understand the consiquences by running this script. I will
    take no responsibility for any damage done by this script."
}

is_not_linux() {
    [[ $(uname -s) != "Linux" ]]
}

# ------------------------------------------------------------------------------

accept='false'

while getopts 'y' flag; do
    case "${flag}" in
        y) accept='true' ;;
        *) print_usage
            exit ;;
    esac
done

if [[ "$accept" == "false" ]]; then
    read -p "This is my setup script which will create symbolic links to my .tmux.conf, .vimrc,
.zshrc, init.vim, .aliases and .terminfo to the current home directory. The
script will move any previously existing dotfile to the ~/old_dotfiles folder
and install needed software.

    ** I will take no responsibility for any damage done by this script. **

Only continue if you fully understand the consequences!
Do you want to continue? (Y/n): " -r
    [[ $REPLY =~ ^[Yy]$  ]] || exit 2
fi

if is_not_linux; then
    echo "Your OS is not supported by this script."
    exit 1
fi

sudo -v

# ------------------------------------------------------------------------------
# Install basic applications

APPLICATIONS=(autoconf build-essential libtool \
    libncurses5-dev libevent-dev fontconfig ripgrep unzip \
    zsh wget curl tree htop neovim \
)

for app in ${APPLICATIONS[*]}; do
    sudo apt install -q -y $app
done

/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/JetBrains/JetBrainsMono/master/install_manual.sh)"
git clone https://github.com/tmux-plugins/tpm.git ~/.tmux/plugins/tpm
curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh

# Zoxide
cargo install zoxide

# ------------------------------------------------------------------------------
# Install oh-my-zsh

sh -c "$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"
git clone https://github.com/zsh-users/zsh-autosuggestions ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-autosuggestions
git clone https://github.com/zsh-users/zsh-syntax-highlighting.git ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-syntax-highlighting

echo "changing default shell"
chsh -s /bin/zsh

# ------------------------------------------------------------------------------
# Copy terminfo

[ -d ~/.terminfo ] || mkdir ~/.terminfo
cp -f ~/Documents/Git/dotfiles/terminfo/tmux.terminfo           ~/.terminfo
cp -f ~/Documents/Git/dotfiles/terminfo/tmux-256color.terminfo  ~/.terminfo
cp -f ~/Documents/Git/dotfiles/terminfo/xterm-256color.terminfo ~/.terminfo

# ------------------------------------------------------------------------------
# Link dotfiles

mkdir ~/old_dotfiles &> /dev/null
mkdir -p ~/.config/nvim

mv -f ~/.vimrc                ~/old_dotfiles/.vimrc
mv -f ~/.tmux.conf            ~/old_dotfiles/.tmux.conf
mv -f ~/.aliases              ~/old_dotfiles/.aliases
mv -f ~/.zshrc                ~/old_dotfiles/.zshrc
mv -f ~/.config/nvim/init.vim ~/old_dotfiles/init.vim

sudo ln -sf ~/Documents/Git/dotfiles/dotfiles/.vimrc     ~/.vimrc
sudo ln -sf ~/Documents/Git/dotfiles/dotfiles/.tmux.conf ~/.tmux.conf
sudo ln -sf ~/Documents/Git/dotfiles/dotfiles/.aliases   ~/.aliases
sudo ln -sf ~/Documents/Git/dotfiles/dotfiles/.zshrc     ~/.zshrc
sudo ln -sf ~/Documents/Git/dotfiles/dotfiles/init.vim   ~/.config/nvim

# ------------------------------------------------------------------------------
# clean system

sudo apt-get clean
sudo apt autoremove

echo -e "\nWow! Such power, much user 💖 🎉"
