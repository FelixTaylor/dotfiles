# @version:       2.0.9
# @since:         2019-12-10
# @last-updated:  2021-11-11
# @author:        Felix Taylor
# @licence        GPLv3

export ZSH=$HOME/.oh-my-zsh

ZSH_THEME= # "af-magic"
ENABLE_CORRECTION="true"
plugins=(git docker zsh-autosuggestions colored-man-pages web-search zsh-syntax-highlighting)

setopt ignoreeof                  # prevent accidental C-d from exiting shell

autoload -Uz vcs_info
precmd() { vcs_info }

#function success_indicator() {
    #if [ $? -eq 0 ]; then
        #echo ""
    #else
        #echo ""
    #fi
#}

zstyle ':vcs_info:git:*' formats '%F{39}%b%f'
export PROMPT='$(whoami)@%1~%(!.%F{160}%%%f.%F{2:}$%f) '
export RPROMPT='${vcs_info_msg_0_} %F{238}%*%f'
#export RPROMPT='${vcs_info_msg_0_} %F{238}%*%f $(success_indicator)'

source ~/.aliases
source $ZSH/oh-my-zsh.sh
source ~/.oh-my-zsh/custom/plugins/zsh-autosuggestions/zsh-autosuggestions.zsh

export PATH=/home/hackerman/.local/bin:$PATH
eval "$(zoxide init zsh)"
