" @version      2.4.21
" @since        2018-12-09
" @last-update  2024-05-16
" @author       Felix Taylor
" @licence      GPLv3

" @info         This is my .vimrc file form (neo)vim.You can also find this
"               .vimrc at github.com/FelixTaylor/dotfiles, feel free to use
"               whatever you find and like from this file.

" =============================================================================

set nocompatible                     " Don't by like vi be VIM
let mapleader = ","                  " Set mapleader
filetype off

" {{{ --- Encoding -------------------------------------------------------------
set encoding=utf-8                   " output in the terminal
set fileencoding=utf-8               " output in file
" }}}

" {{{ --- Folding --------------------------------------------------------------
set foldenable						 " enable folding
set foldmethod=marker                " Set default fold method
set foldlevelstart=1                 " all tabs are closed on file load
set foldnestmax=5                    " 5 nested fold max
set foldcolumn=2                     " display folds on left-hand side
" }}}

" {{{ --- Misc -----------------------------------------------------------------
set so=6                             " Set 6 lines to the cursor
set autoread                         " Set auto read when a file is changed
                                     " from the outside
"set list                            " Deactivated, but this option can easily
                                     " be toggled with: ':set list!'

set listchars=eol:¬,tab:>·,trail:~,extends:>,precedes:<,space:·
set backspace=indent,eol,start       " Enables deletion of indets, eol's, start
set noerrorbells visualbell t_vb=    " vim.fandom.com/wiki/Disable_beeping
set whichwrap+=<,>,h,l
set splitbelow splitright            " This will always split the screen below
                                     " or right. I found this command over at
                                     " vi.stackexchange.com/questions/16795/
set updatetime=300

" Tab completion
" will insert tab at beginning of line, will use completion if not at beginning
set wildmode=list:longest,list:full
function! InsertTabWrapper()
    let col = col('.') - 1
    if !col || getline('.')[col - 1] !~ '\k'
        return "\<Tab>"
    else
        return "\<C-n>"
    endif
endfunction
inoremap <Tab> <C-r>=InsertTabWrapper()<CR>
" }}}

" {{{ --- Search ---------------------------------------------------------------
set completeopt=menu,menuone,noinsert,noselect " Enable completion menu
set wildignore=*.swp,*.git,*.DS_Store " Files to ignore
set hlsearch                          " highlight matches
set incsearch                         " search as characters are entered
set ignorecase
set showmatch
set wildmenu                          " Display all matching files
set path+=**                          " Search down into subfolders Provides
                                      " tab-completion for file-related tasks
" }}}

" {{{ --- Indents --------------------------------------------------------------
set smarttab
set shiftwidth=4
set tabstop=4                        " number of visual spaces per TAB
set expandtab                        " insert spaces if TAB is pressed
filetype plugin indent on            " load filetype-specific indent files
set ai                               " Auto indent
set si                               " Smart indent
" }}}

" {{{ --- Appearance -----------------------------------------------------------
syntax enable                        " enable syntax processing
set guioptions-=L                    " disable gui scrollbar
set cursorline                       " highlight current line
set number                           " Show current line number
set relativenumber                   " Show relative line numbers
set noemoji                          " Somehow this option helps to display
                                     " emojis!?: youtube.com/watch?v=F91VWOelFNE
set t_Co=256
set background=dark
let &colorcolumn=join(range(81,999),",")
highlight ColorColumn ctermbg=237 guibg=#3c3836

if has("nvim")
    set termguicolors
    let g:limelight_conceal_ctermfg = 240
    let g:limelight_conceal_guifg = '#3c3836'
    let g:limelight_default_coefficient = 1

    let g:any_jump_window_width_ratio  = 0.3
    let g:any_jump_window_height_ratio = 0.6
    let g:any_jump_window_top_offset   = 4
    let g:any_jump_list_numbers = 0
    let g:any_jump_remove_comments_from_results = 1
    let g:any_jump_ignored_files = ['*.tmp', '*.temp', '*.swp']
    let g:any_jump_results_ui_style = 'filename_first'

    let nerdtreedirarrows = 1
    let g:ctrlp_custom_ignore = '\v[\/]\.(git|hg|svn)$'
    let g:ctrlp_user_command = ['.git', 'cd %s && git ls-files -co --exclude-standard']

    let g:gruvbox_italic = 1
    let g:gruvbox_contrast_dark = "hard"
    let g:airline_theme = "gruvbox"
    colorscheme gruvbox

endif
" }}}

" {{{ --- Key bindings ---------------------------------------------------------
inoremap jj <Esc>
nnoremap <leader>o :source ~/.vimsession/default.vim<CR>
nnoremap <leader>s :Obsess ~/.vimsession/default.vim<CR>
nnoremap <leader>u :UndotreeToggle<CR>
nnoremap <leader>t :NERDTreeToggle<CR>
nnoremap <leader>l :Limelight!!<CR>
nnoremap <leader>w :w<CR>
nnoremap <leader>q :q<CR>
nnoremap <leader>wq :wq<CR>
nnoremap <leader>/ :nohl<CR>

" https://github.com/r00k/dotfiles/blob/master/vimrc
" Disable that goddamn 'Entering Ex mode. Type 'visual' to go to Normal mode.'
" that I trigger 40x a day.
map Q <Nop>

" auto-center after movement
nmap G Gzz
nmap n nzz
nmap } }zz
nmap { {zz

" Fast switch between splits
" Not working with split terminal
nmap <C-j> <C-W>j
nmap <C-k> <C-W>k
nmap <C-h> <C-W>h
nmap <C-l> <C-W>l

" }}}

" {{{ --- autocmd --------------------------------------------------------------
if has('autocmd')
    " Found at: github.com/thoughtbot/dotfiles/blob/master/vimrc
    augroup vimrcEx
        autocmd!
        " When editing a file, always jump to the last known cursor position.
        " Don't do it for commit messages, when the position is invalid, or when
        " inside an event handler (happens when dropping a file on gvim).
        autocmd BufReadPost *
        \ if &ft != 'gitcommit' && line("'\"") > 0 && line("'\"") <= line("$") |
        \     exe "normal g`\"" |
        \ endif
    augroup END

    autocmd FileType * autocmd BufWritePre <buffer> %s/\s\+$//e
    autocmd BufEnter * silent! lcd %:p:h " Set the working path of vim to the
                                         " current directory of the opend file.
                                         " Found that on: vim.wikia.com

    augroup pandoc_syntax
        au! BufNewFile,BufFilePre,BufRead *.md set filetype=markdown.pandoc
        au! BufNewFile,BufFilePre,BufRead *.markdown set filetype=markdown.pandoc
    augroup END
endif
" }}}

