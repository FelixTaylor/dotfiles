" @version      1.0.5
" @since        2020-03-06
" @last update  2023-12-18
" @author       Felix Taylor
" @licence      GPLv3
"
" @info         This is my init.vim file for neovim and its part of my config
"               files which you can find over at gitlab.com/FelixTaylor/dotfiles.

" Found this at: gist.github.com/nkakouros/e7f308662a80e7fc54c549ff35f8050d
if empty(glob('~/.local/share/nvim/site/autoload/plug.vim'))
  silent !curl -fLo ~/.local/share/nvim/site/autoload/plug.vim --create-dirs
    \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
  augroup VimPlugAutoInstall
    autocmd!
    autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
  augroup END
endif

set runtimepath^=~/.vim runtimepath+=~/.vim/after
let &packpath = &runtimepath

call plug#begin('~/.vim/plugged')


Plug 'artur-shaik/vim-javacomplete2'
Plug 'airblade/vim-gitgutter'
Plug 'christoomey/vim-tmux-navigator'
Plug 'ctrlpvim/ctrlp.vim'
Plug 'ekalinin/Dockerfile.vim'
Plug 'jiangmiao/auto-pairs'
Plug 'junegunn/limelight.vim'
Plug 'mbbill/undotree'
Plug 'morhetz/gruvbox'
" Plug 'neoclide/coc.nvim', {'branch': 'release'}
Plug 'pechorin/any-jump.vim'
Plug 'tpope/vim-obsession'
Plug 'RyanMillerC/better-vim-tmux-resizer'
Plug 'scrooloose/nerdcommenter'
Plug 'scrooloose/nerdtree'
Plug 'vim-airline/vim-airline'
Plug 'vimwiki/vimwiki'
Plug 'vim-pandoc/vim-pandoc-syntax'
Plug 'wincent/terminus'
Plug 'ryanoasis/vim-devicons'
Plug 'rust-lang/rust.vim'

call plug#end()
source ~/.vimrc

