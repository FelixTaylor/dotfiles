# @version       1.3.6
# @since         2019-05-22
# @last update   2021-03-24
# @author        Felix Taylor
# @licenc        GPLv3

# @info          This is my tmux.conf file currently in use. I'm just getting
#                started with tmux, therefore this file contains settings which
#                I have copied from other tmux.conf files. See the comments to
#                find a link to the source.

set-environment -g PATH "/usr/local/bin:/bin:/usr/bin"

# set zsh as default shell
set -g default-command /bin/zsh
set -g default-shell /bin/zsh

# change ctrl-b to ctrl-a
# Maybe I want to change this to spacebar or something freaky
unbind C-b
set -g prefix C-a

# split panes using | and -
bind | split-window -h
bind - split-window -v
unbind '"'
unbind %

set -g mouse on
bind -n WheelUpPane if-shell -F -t = "#{mouse_any_flag}" "send-keys -M" "if -Ft= '#{pane_in_mode}' 'send-keys -M' 'copy-mode -e; send-keys -M'"

# set first window to index 1 (not 0)
set -g base-index 1
set -g pane-base-index 1

# reload tmux config with ctrl + a, r
unbind r
bind r \
	source-file ~/.tmux.conf \;\
	display 'Reloaded tmux config.'

# Status bar settings
# found this over at gist.github.com/rajanand02/9407361
set -g status-bg colour235
set -g status-justify centre
set -g status-left-length 100
set -g status 'on'
setw -g window-status-separator ''
set-option -g status-position top

# don't allow tmux to rename the window based on commands running
set-window-option -g allow-rename off
set-option -ga terminal-overrides ",tmux-256color-italic:Tc"
set -g default-terminal "xterm-256color"

set -g status-left '#[fg=colour232,bg=colour154] #W #[fg=colour154,bg=colour235,nobold,nounderscore,noitalics]#[fg=colour121,bg=colour235] #(whoami) '
setw -g window-status-current-format '#[fg=colour235,bg=colour238,nobold,nounderscore,noitalics]#[fg=colour222,bg=colour238] #I  #W ♥ #[fg=colour238,bg=colour235,nobold,nounderscore,noitalics]'
set -g status-right '#[fg=colour235,bg=colour235,nobold,nounderscore,noitalics]#[fg=colour121,bg=colour235] %H:%M  %a #[fg=colour154,bg=colour235,nobold,nounderscore,noitalics]#[fg=colour232,bg=colour154] %Y-%m-%d '

setw -g window-status-format '#[fg=colour235,bg=colour235,nobold,nounderscore,noitalics]#[default] #I  #W #[fg=colour235,bg=colour235,nobold,nounderscore,noitalics]'

## Plugins
set -g @tpm_plugins '                           \
    tmux-plugins/tpm                            \
    christoomey/vim-tmux-navigator              \
    tmux-plugins/tmux-resurrect                 \
    tmux-plugins/tmux-continuum                 \
    RyanMillerC/better-vim-tmux-resizer         \
'
## Plugin variables
set -g @continuum-restore 'on' # Auomatically resore enviornment

# keep this line at the very bottom!
run '~/.tmux/plugins/tpm/tpm'
