# Docker Cheat Sheet

### New image

Build new docker image:

```bash
docker build -t <image-name> .
```

Run docker image:

```bash
docker run -it <image-name>
```

### Display information

List docker process:

```bash
docker ps -a
```

List docker images:

```bash
docker images -a
```

Inspect docker image:

```bash
docker inspect <image-name>
```

Search DockerHub

```bash
docker search <name>
```

### Remove images

Delete docker image:

```bash
docker rmi <image-name>
```

Remove unneeded containers, volumes and networks

```bash
docker system prune -a
```

### Docker volumes

Create volume

```bash
docker volume create
```

_(from docker.com)_

When you create a volume, it is stored within a directory on the Docker host.
When you mount the volume into a container, this directory is what is mounted
into the container.

A given volume can be mounted into multiple containers simultaneously. When no
running container is using a volume, the volume is still available to Docker and
is not removed automatically. You can remove unused volumes using

Delete volume

```bash
docker volume prune
```
