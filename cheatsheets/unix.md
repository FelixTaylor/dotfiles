# UNIX

### Search 

```shell
grep    -i      # case insensitive
        -n      # show linenumbe
        -e      # multiple seachr terms
        -c      # only show row count
        -v      # all lines without
```

### Processes

```shell
ps      -e      # List processes from all users
        -f      # extendet listing
```

### Sort

```shell
sort    -r      # sort descending
        -t ":"  # assign ':' as delimiter
        -n      # sort numerically
        -u      # suppress duplicate lines
        -k n,m  # sort from 'n' to 'm'
```

### Files

```shell
name -f         # File exists and is a 'normal' file
name -d         # File exists but it's a directory
mame -s         # File exists but it's not readable
```

### Condition

```shell
var1 -eq var2   # variables are equal
var1 -ne var2   # variables are not equal
var1 -le var2   # variable 1 is less or equal
var1 -lt var2   # variable 1 is less
var1 -ge var2   # variable 1 is gerater or equal
var1 -gt var2   # variable 1 is greater
```