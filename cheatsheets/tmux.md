# tmux cheatsheet

Start a new session with a name:
```
$ tmux new -s [name]
```

Start an attached session:
```
$ tmux attach #
$ tmux attach -t [name]
$ tmux a -t [name]
```

Attach to last session
```
$ tmux a
```

List all tmux sessions:
```
$ tmux ls
$ ctrl+b s
```

## Window Handling
| Command          | Keybinding   |
|:-----------------|:-------------|
| New window       | `<prefix>+c` |
| Next Window      | `<prefix>+n` |
| Previous Window  | `<prefix>+p` |
| List all windows | `<prefix>+w` |
| Rename a window  | `<prefix>+,` |

Note: A better option is: [vim-tmux-navigator](https://github.com/christoomey/vim-tmux-navigator)


## Pane Handling
| Command                  | Keybinding           |
|:-------------------------|:---------------------|
| Split panes vertically   | `<prefix>+%`         |
| Split panes horizontally | `<prefix>+"`         |
| Kill pane                | `<prefix>+x`         |
| Show pane numbers        | `<prefix>+q`         |
| Move current pane left   | `<prefix>+{`         |
| Move current pane right  | `<prefix>+}`         |

## Ressurect Key bindings
| Command                  | Keybinding           |
|:-------------------------|:---------------------|
| Kill session             | tmux kill-server     |
| Save session             | `<prefix> + Ctrl-s`  |
| Restore session          | `<prefix> + Ctrl-r`  |

-------------------------------------------------------------------------------
_2020-03-28_
