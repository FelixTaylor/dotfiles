# Kubernetes Cheat-Sheet

_The sudo command was omitted in this cheat-sheet._

Start minikube with no vm driver (this is linux specific). Also the cpus and
memory values should be reviewed and adjusted.

```bash
minikube start --vm-dirver=none --cpus 10 --memory 12000
# or minikube start --vm-driver=docker
```

Make `none` the default driver:
_More infos at: https://minikube.sigs.k8s.io/docs/drivers/none/_

```bash
minikube config set driver none
```

Apply a new .yaml kluster file

```bash
kubectl apply -f filename.yaml
```

ZSH completion for kubernetes
```bash
echo "[[ $commands[kubectl] ]] && source <(kubectl completion zsh)" >> ~/.zshrc
```

Possible properties:
name, image, command, args, workingDir, ports env, resources, volumeMounts,
livenessProbe, readinessProb, lifecycle, terminationMessagePath, imagePullPolicy
securityContext, stdin, stdinOnce, tty

--------------------------------------------------------------------------------

## Get information

```bash
kubectl get pods                          # List all kubernetes pods
kubectl get namespace                     # List all namespaces
kubectl describe pod <name>               # Show pod information
kubectl config view                       # List configuration
kubectl get deployments --all-namespaces  # List all deployments
kubectl get deployments -n default        # List all deployments from the
                                          # default namespace
kubectl get secret                        # list all secrets
kubectl get secret <name>                 # list specific secret
```

Filter the output of the 'get command' by appending the -l parameter followed
by the key value pair of interest.

```bash
kubectl get pods -l <key>=<value>
```

--------------------------------------------------------------------------------

## Delete Pods

```bash
kubectl delete deployment <NAME>
```

Note: Delete the deployment not the pod or service, otherwise they will come
back to life like zombies but with a different suffix.

--------------------------------------------------------------------------------

## Admission Control

> For the follwoing commands to work like expected first install kubeadm:
> https://snapcraft.io/kube-apiserver

Start kubernetes API server with specified admission.

```bash
kube-apiserver --enable-admission-plugins=NamespaceLifecycle,ResourceQuota, \
    PodSecurityPolicy,DefaultStorageClass
```

To see which admission plugins are enabled:

```bash
kube-apiserver -h | grep enable-admission-plugins
```

Create role or even a complete server from file:

```bash
kubectl create -f <name>.yaml
```

See all certificates:

```bash
sudo kubectl get csr
```

Approve certificate:

```bash
kubectl certificate approve <name>
```

Extract the approved certificate from the certificate signing request, decode
it with base64 and save it as a certificate file. _Note:_ base64 is **no**
decription, it's an encoding.

```bash
kubectl get csr <name>-csr -o jsonpath='{.status.certificate}' | \
    base64 --decode > <name>.crt
```

Configure the user's credentials by assigning the key and certificate:

```bash
kubectl config set-credentials <name> --client-certificate=<name>.crt \
    --client-key=<name>.key
```

Create secret:
```bash
kubectl create secret generic <name> --from-literal=password=<password>
```

--------------------------------------------------------------------------------

Add custom namespace

```bash
kubectl create namespace <name>
```


SSH-Connection to minikube

```bash
minikube ssh
```

Find the config file at ~/.kube/config or by using following command:

```bash
kubectl config view
```

