# Vim cheat sheet

In this file I'll write down some more or less usefull vim tricks which I found
all over the world wide web.

| Command  | Description                                        |
| -------- | -------------------------------------------------- |
| `ctrl+o` | Run single command in insert mode                  |
| `K`      | Tries to open `man` pages for command under cursor |

### Scrollbinding
```viml
:set scrollbind     " set vims scrollbind
:set scb!           " toggles vims scrollbind
```

### Changing words in file
```viml
:s/old/new/g        " changes all words 'old' to 'new' in this line
:1,3s/old/new/g     " same as above but only includes line 1 to 3
:%s/old/new/g       " same as above but changes word in entire file
```

### Deleting
```viml
ctrl-h              " delete back one character
ctrl-w              " delete back one word
ctrl-u              " delete back to start of line
```

