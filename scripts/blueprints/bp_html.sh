#!/bin/bash

# @version       1.0.2
# @author        Felix Taylor
# @since         2019-12-17
# @last updated  2021-03-07
# @licence       GPLv3
#
# @info          This script will generate a simple html page.

main() {
    source ~/Documents/Git/dotfiles/scripts/bash/check_parameter_present.sh $1
    printf '<!DOCTYPE html>

<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title>'$1'</title>
        <link rel="stylesheet" href="css/main.css" />
    </head>
    <body>
        <div id="wrapper">
        </div>
    </body>
    <script src="js/main.js"></script>
</html>
' > "$PWD/$1.html"
}

main $1

