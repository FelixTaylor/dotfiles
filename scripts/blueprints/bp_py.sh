#!/bin/bash

# @version       1.1.3
# @author        Felix Taylor
# @since         2019-12-19
# @last updated  2021-03-10
# @licence       GPLv3
#
# @info          The bp_py script will generate an new python class.

main() {
    source ~/Documents/Git/dotfiles/scripts/bash/check_parameter_present.sh $1
    printf '#!/usr/bin/env python

# @version       1.0.0
# @author        Felix Taylor
# @since         '$(date +"%Y-%m-%d")'
# @last updated  '$(date +"%Y-%m-%d")'
# @licence       GPLv3
#
# @info

class '$1'(objct):
    def __init__(self):
        # todo

' > "$PWD/$1.py"
    chmod +x "$PWD/$1.py"
}

main $1
