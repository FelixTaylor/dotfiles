#!/bin/bash

# @version       1.0.5
# @author        Felix Taylor
# @since         2019-12-17
# @last updated  2021-03-10
# @licence       GPLv3
#
# @info          This script will generate a simple cpp file.

main() {
    source ~/Documents/Git/dotfiles/scripts/bash/check_parameter_present.sh $1
    printf '#include <iostream>

// @version       1.0.0
// @author        Felix Taylor
// @since         '$(date +"%Y-%m-%d")'
// @last updated  '$(date +"%Y-%m-%d")'
// @licence       GPLv3
//
// @info

int main() {

    return 0;
}
' > "$1.cpp"
}

main $1

