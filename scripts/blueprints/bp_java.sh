#!/bin/bash

# @version       1.0.1
# @author        Felix Taylor
# @since         2019-12-19
# @last updated  2021-03-07
# @licence       GPLv3
#
# @info

main() {
    source ~/Documents/Git/dotfiles/scripts/bash/check_parameter_present $1
    printf '/*
* @version       1.0.0
* @author        Felix Taylor
* @since         '$(date +"%Y-%m-%d")'
* @last updated  '$(date +"%Y-%m-%d")'
* @licence       GPLv3
*
* @info
*/
public class '$1' {

	public static void main(String[] args) {

    }
}
' > "$1.java"
}

main $1
