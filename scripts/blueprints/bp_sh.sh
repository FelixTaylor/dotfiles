#!/bin/bash

# @version       1.0.4
# @author        Felix Taylor
# @since         2019-12-17
# @last updated  2021-03-08
# @licence       GPLv3
#
# @info          This is a script which makes a new .sh file and adds the
#                shebang and a few informations at the top including the
#                version, since when the file exists, the date it was last
#                changed and the author.

main() {
    source ~/Documents/Git/dotfiles/scripts/bash/check_parameter_present.sh $1
    printf '#!/bin/bash

# @version       1.0.0
# @author        Felix Taylor
# @since         '$(date +"%Y-%m-%d")'
# @last updated  '$(date +"%Y-%m-%d")'
# @licence       GPLv3
#
# @info

main() {
    # todo
}

main
' > "$PWD/$1.sh"
    chmod +x "$PWD/$1.sh"
}

main $1
