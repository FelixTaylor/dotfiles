#!/bin/bash

# @version       1.1.0
# @author        Felix Taylor
# @since         2019-12-17
# @last updated  2021-03-07
# @licence       GPLv3
#
# @info          The bp_scss script will generate a basic scss file to use in
#                html projects.

main() {
    source ~/Documents/Git/dotfiles/scripts/bash/check_parameter_present.sh $1
    printf '/*
* @version       1.0.0
* @author        Felix Taylor
* @since         '$(date +"%Y-%m-%d")'
* @last updated  '$(date +"%Y-%m-%d")'
* @licence       GPLv3
*/

body {

    #wrapper {
    }
}
' > "$1.scss"

}

main $1

