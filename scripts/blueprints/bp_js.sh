#!/bin/bash

# @version       1.0.1
# @author        Felix Taylor
# @since         2019-12-19
# @last updated  2021-03-07
# @licence       GPLv3
#
# @info          The bp_js script will generate a new javascript file with also
#                the 'ready' function in it.

main() {
    source ~/Documents/Git/dotfiles/scripts/bash/check_parameter_present.sh $1
    printf '
// @version       1.0.0
// @author        Felix Taylor
// @since         '$(date +"%Y-%m-%d")'
// @last updated  '$(date +"%Y-%m-%d")'
// @licence       GPLv3
//
// @info

function ready(fn) {
    if (document.ready == "loading") {
        document.addEventListener("DOMContentLoaded", fn);
    } else {
        fn();
    }
}

ready(function() {
    console.log("ready!");
});

' > "$1.js"

}

main $1
