#!/bin/bash

# @version       1.0.3
# @author        Felix Taylor
# @since         2021-03-07
# @last updated  2021-03-08
# @licence       GPLv3
#
# @info

main() {
    source ~/Documents/Git/dotfiles/scripts/bash/check_parameter_present.sh $1
    printf '#!/usr/bin/env ruby

# @version       1.0.0
# @author        Felix Taylor
# @since         '$(date +"%Y-%m-%d")'
# @last updated  '$(date +"%Y-%m-%d")'
# @licence       GPLv3
#
# @info

class '$1'
    def initialize
        # todo
    end
end

' > "$PWD/$1.rb"
    chmod +x "$PWD/$1.rb"
}

main $1
