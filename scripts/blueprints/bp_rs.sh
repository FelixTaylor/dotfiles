#!/bin/bash

# @version       1.0.0
# @author        Felix Taylor
# @since         2021-10-03
# @last updated  2021-10-03
# @licence       GPLv3
#
# @info          This script will generate a simple rust file

main() {
    source ~/Documents/Git/dotfiles/scripts/bash/check_parameter_present.sh $1
    printf '
// @version       1.0.0
// @author        Felix Taylor
// @since         '$(date +"%Y-%m-%d")'
// @last updated  '$(date +"%Y-%m-%d")'
// @licence       GPLv3
//
// @info

fn main() {
    // todo
}
' > "$1.rs"
}

main $1

