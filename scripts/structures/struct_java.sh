#!/bin/bash

# @version       1.0.2
# @author        Felix Taylor
# @since         2019-12-19
# @last updated  2021-03-07
# @licence       GPLv3
#
# @info          The scruct_java script generates a src and a bin folder inside
#                of a folder with the given parameter string. Also a Main.java
#                file will be placed inside of the src folder.

main() {
    source ~/Documents/Git/dotfiles/scripts/bash/check_parameter_present.sh $1
    mkdir -p "$1/src/main" "$1/src/test" "$1/bin"
    source ~/Documents/Git/dotfiles/scripts/blueprints/bp_java.sh "$1/src/Main"
}

main $1
