#!/bin/bash

# @version       1.0.3
# @author        Felix Taylor
# @since         2019-12-17
# @last updated  2021-03-07
# @licence       GPLv3
#
# @info          The struct_html script will generate a basic standard html
#                folder structur including a page, a css and a js folder and a
#                index.html, a main.scss and a main.js file.

main() {
    source ~/Documents/Git/dotfiles/scripts/bash/check_parameter_present.sh $1
    mkdir -p "$1/pages" "$1/css" "$1/js" "$1/images"
    source ~/Documents/Git/dotfiles/scripts/blueprints/bp_html.sh "$1/index";
    source ~/Documents/Git/dotfiles/scripts/blueprints/bp_scss.sh "$1/css/main";
    source ~/Documents/Git/dotfiles/scripts/blueprints/bp_js.sh   "$1/js/main"
}

main $1

