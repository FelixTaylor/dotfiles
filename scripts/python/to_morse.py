# @version       1.0.0
# @since         2019-08-20
# @last updated  2019-08-20
# @author        Felix Taylor
#
# @info         Small Command Line Interface which will translate the given input
#               String into morse code. Note that this cli currently only
#               translate A-Z, 0-9, # ' ' <space> and punctuation like:
#               . , : ; - + = ! ?

import argparse
import re
import sys

parser = argparse.ArgumentParser("morse.py")
parser.add_argument("text", help="The input text which will be translated into morse code.")
args = parser.parse_args()

alphabet = {
        "A":".-",   "B":"-...", "C":"-.-.", "D":"-..",  "E":".",    "F":"..-.",
        "G":"--.",  "H":"....", "I":"..",   "J":".---", "K":"-.-",  "L":".-..",
        "M":"--",   "N":"-.",   "O":"---",  "P":".--.", "Q":"--.-", "R":".-.",
        "S":"...",  "T":"-",    "U":"..-",  "V":"...-", "W":".--",  "X":"-..-",
        "Y":"-.--", "Z":"--..",

        "1":".----", "2":"..---", "3":"...--", "4":"....-", "5":".....",
        "6":"-....", "7":"--...", "8":"---..", "9":"----.", "0":"-----",

        " ":".......", ".":".-.-.-", ",":"--..--", "?":"..--..", "!":"-.-.--",
        "'":".----.", ":":"---...", ";":"-.-.-.", "=":"-...-", "+":".-.-.",
        "-":"-....-"
        }

morse = {v: k for k, v in alphabet.items()}

def translate():
    to_morse=""
    for i in sys.argv[1]:
        to_morse = to_morse + alphabet[i.upper()]
    print(to_morse)

translate()

