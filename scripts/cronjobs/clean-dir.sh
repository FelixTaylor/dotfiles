#!/bin/bash

# @version       1.0.6
# @author        Felix Taylor
# @since         2020-07-24
# @last updated  2021-04-03
# @licence       GPLv3
#
# @info          This script will go threw all files in the specified directory
#                and move all known files to it's designated place. For example
#                all MP3 files will be moved to ~/Music (default location).
#                The script will place a log file in the ~/Documents/logs/
#                directory stating all movements that occured.
#
#                Note that you have to change the value of USER variable in
#                order for this script to function.
#
#                Note that I will not take any resposibility for any damage
#                done by this script.
#
# @param         $1) the directory which is being cleand
#
# @vars          The locations to which the script will move the files can be
#                customized by altering following variables.

LOG_DIR=~/Documents/logs
DOCUMENTS=~/Documents
MUSIC=~/Music
VIDEOS=~/Videos
IMAGES=~/Pictures


# @info          The move function moves the current file to the appropiate new
#                location and logs the action.
#
# @param         $1) the file which is being moved
#                $2) the designated path
move() {
    local log=$LOG_DIR/cronjobs.log
    local target=$2/$(basename -- $1)
    local time=$(date +"%Y-%m-%d,%H:%M")
    local error_msg=$( mv $1 $target 2>&1 )
    echo $time";"$1";"$2";"$error_msg >> $log
}


# @info          The target function returns the appropiate path for the current
#                file by looking at the suffix.
#
# @param         $1) the file which is being moved
target() {
    case $1 in
        *.mp3 | *.flac )                         echo $MUSIC     ;;
        *.avi | *.mp4  )                         echo $VIDEOS    ;;
        *.txt | *.pdf  | *.xml | *.md  )         echo $DOCUMENTS ;;
        *.jpg | *.jpeg | *.png | *.gif | *.bmp ) echo $IMAGES    ;;
    esac
}


main() {
    [[ $1 == "" ]] && echo "Exit $0 script: No directory provided." &&  exit 1;
    [[ -d $LOG_DIR ]] || mkdir $LOG_DIR

    for file in $1/*; do
        local to_dir=$(target $file)
        move $file $to_dir
    done

    rm /home/$USER/Downloads/*
}


main $1
