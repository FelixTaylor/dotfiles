# Cron

The cron jobs in this directory are doing various tasks like moving or
deleting files from specific folders.

**Do not run any of these scripts if you do not fully understand the**
**consiquences.**

I have added the scripts to my crontab by calling:

```bash
$ crontab -e
```

It is also possible to load the cron tasks from a text file like so:

```bash
crontab /path/to/cron-backup.txt
```

In that file I have added tasks like:

```bash
0 9,21 * * mon /path/to/clean-dir.sh /home/<user>/Downloads
```

---

_(2020-07-25)_

