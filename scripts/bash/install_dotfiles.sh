#!/bin/bash

# @version       1.0.0
# @author        Felix Taylor
# @since         2023-02-21
# @last updated  2023-02-21
# @licence       GPLv3
#
# @info          This script moves all needed dotfiles to the home directory.

main() {
    mkdir ~/.old_dotfiles &> /dev/null
    mkdir -p ~/.config/nvim

    mv -f ~/.vimrc                ~/.old_dotfiles/.vimrc
    mv -f ~/.tmux.conf            ~/.old_dotfiles/.tmux.conf
    mv -f ~/.aliases              ~/.old_dotfiles/.aliases
    mv -f ~/.zshrc                ~/.old_dotfiles/.zshrc
    mv -f ~/.config/nvim/init.vim ~/.old_dotfiles/init.vim

    sudo ln -sf ~/Documents/Git/dotfiles/dotfiles/.vimrc     ~/.vimrc
    sudo ln -sf ~/Documents/Git/dotfiles/dotfiles/.tmux.conf ~/.tmux.conf
    sudo ln -sf ~/Documents/Git/dotfiles/dotfiles/.aliases   ~/.aliases
    sudo ln -sf ~/Documents/Git/dotfiles/dotfiles/.zshrc     ~/.zshrc
    sudo ln -sf ~/Documents/Git/dotfiles/dotfiles/init.vim   ~/.config/nvim
}

main
