#!/bin/bash

# @version       1.0.0
# @author        Felix Taylor
# @since         2023-02-21
# @last updated  2023-02-21
# @licence       GPLv3
#
# @info          This script installs zshell and sets it as default.

main() {
    sh -c "$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"
    git clone https://github.com/zsh-users/zsh-autosuggestions ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-autosuggestions
    git clone https://github.com/zsh-users/zsh-syntax-highlighting.git ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-syntax-highlighting
    echo "changing default shell"
    chsh -s /bin/zsh
}

main
