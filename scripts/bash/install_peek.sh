#!/bin/bash

# @version       1.0.0
# @author        Felix Taylor
# @since         2023-02-21
# @last updated  2023-02-21
# @licence       GPLv3
#
# @info          This script installs the peek application.

main() {
    sudo add-apt-repository ppa:peek-developers/stable
    sudo apt install peek
}

main
