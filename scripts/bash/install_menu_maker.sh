#!/bin/bash

# @version       1.0.1
# @author        Felix Taylor
# @since         2021-03-14
# @last updated  2021-03-14
# @licence       GPLv3
#
# @info          This script builds and installs 'menu maker'

MENU_MAKER=menumaker-0.99.12

main() {
    sudo -v
    sudo apt install xterm rofi lxappearance tint2 nitrogen &> #/dev/null
    sudo unzip ~/Documents/Git/dotfiles/software/$MENU_MAKER -d ~/Documents/Git/dotfiles/software/
    cd ~/Documents/Git/dotfiles/software/$MENU_MAKER
    sudo ./configure && sudo make && sudo make install
    sudo mmaker openbox -t xterm -f
    sudo rm -rf ~/Documents/Git/dotfiles/software/$MENU_MAKER
}

main
