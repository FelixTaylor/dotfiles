#! /bin/bash

# @version       1.0.1
# @author        Felix Taylor
# @since         2021-02-28
# @last updatet  2021-02-28
# @licence       GPLv3
#
# @info          A script which pings google to check the internet connection.

ping -4 -c 1 -D -n www.google.de | sed -n 2p \
    >> /home/pi/Documents/scripts/output/ping.out 2>&1

