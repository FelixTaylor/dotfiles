#!/bin/bash

# @version       1.0.0
# @author        Felix Taylor
# @since         2023-02-21
# @last updated  2023-02-21
# @licence       GPLv3
#
# @info          This script installs all needed base application found at apt.

main() {
    APPLICATIONS=(autoconf automake cmake build-essential libtool   \
        libncurses5-dev libevent-dev fontconfig ripgrep unzip pandoc g++ \
        ruby zsh wget curl tmux tree htop cron neovim                    \
    )

    for app in ${APPLICATIONS[*]}; do
        sudo apt install -y $app
    done
}

main
