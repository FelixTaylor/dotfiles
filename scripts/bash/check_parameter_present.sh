#!/bin/bash

# @version       1.0.0
# @author        Felix Taylor
# @since         2021-03-08
# @last updated  2021-03-08
# @licence       GPLv3
#
# @info          This script returns an error code (10) if no parameter was
#                defined.

main() {
    if [[ $1 == "" ]]; then
        echo "Exit script because of error: No file name given!"
        exit 10;
    fi
}

main $1
