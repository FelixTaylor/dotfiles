#!/bin/bash

# @version       1.0.1
# @author        Felix Taylor
# @since         2021-03-19
# @last updated  2023-02-21
# @licence       GPLv3
#
# @info

main() {
    sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(. /etc/os-release; echo "$UBUNTU_CODENAME") stable"
    sudo apt install docker-ce docker-compose
    sudo usermod -aG docker $USER
}

main
