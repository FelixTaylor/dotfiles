#!/bin/bash

# @version       1.0.0
# @author        Felix Taylor
# @since         2023-02-21
# @last updated  2023-02-21
# @licence       GPLv3
#
# @info          This script installs the rust programing language.

main() {
    curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh
}

main
