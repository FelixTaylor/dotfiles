#!/bin/bash

# @version       1.0.0
# @author        Felix Taylor
# @since         2023-02-21
# @last updated  2023-02-21
# @licence       GPLv3
#
# @info          This script installs the nordvpn client.

main() {
    sh <(curl -sSf https://downloads.nordcdn.com/apps/linux/install.sh)
    sudo usermod -aG nordvpn $USER
}

main
