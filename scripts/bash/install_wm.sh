#!/bin/bash

# @version       1.0.0
# @author        Felix Taylor
# @since         2021-03-14
# @last updated  2021-03-14
# @licence       GPLv3
#
# @info          This script installs openbox, compton, nitrogen and tint2.
#                The config files are linked to those in the
#                ~/Documents/Git/dotfiles/dotfiles/.config/ directory.

main() {
    sudo -v
    mkdir -p ~/.config/openbox ~/.config/tint2
    sudo apt install -y openbox obconf compton nitrogen
    sudo ln -sf ~/Documents/Git/dotfiles/dotfiles/.config/openbox/autostart ~/.config/openbox/autostart
    sudo ln -sf ~/Documents/Git/dotfiles/dotfiles/.config/openbox/rc.xml ~/.config/openbox/rc.xml
    sudo ln -sf ~/Documents/Git/dotfiles/dotfiles/.config/tint2/my.tint2rc ~/.config/tint2/my.tint2rc
    source ~/Documents/Git/dotfiles/scripts/bash/install_menu_maker.sh -A
}

main
