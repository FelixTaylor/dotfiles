#!/bin/bash

# @version       1.0.1
# @author        Felix Taylor
# @since         2023-02-21
# @last updated  2023-03-21
# @licence       GPLv3
#
# @info          This script installs the uLauncher application.

main() {
    sudo add-apt-repository ppa:agornostal/ulauncher
    sudo apt update
    sudo apt install ulauncher
}

main
