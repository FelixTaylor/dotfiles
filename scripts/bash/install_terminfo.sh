#!/bin/bash

# @version       1.0.0
# @author        Felix Taylor
# @since         2023-02-21
# @last updated  2023-02-21
# @licence       GPLv3
#
# @info          This script applies the right colours to tmux

main() {
    # todo
    [ -d ~/.terminfo ] || mkdir ~/.terminfo
    cp -f ~/Documents/Git/dotfiles/terminfo/tmux.terminfo           ~/.terminfo
    cp -f ~/Documents/Git/dotfiles/terminfo/tmux-256color.terminfo  ~/.terminfo
    cp -f ~/Documents/Git/dotfiles/terminfo/xterm-256color.terminfo ~/.terminfo
}

main
