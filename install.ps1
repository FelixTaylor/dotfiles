Set-ExecutionPolicy Bypass -Scope Process -Force; # Maybe has to be run manually.
Start-Process Powershell -Verb runAs

[System.Net.ServicePointManager]::SecurityProtocol = [System.Net.ServicePointManager]::SecurityProtocol -bor 3072; 
Invoke-Expression ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))

choco install discord -y
choco install firefox -y
choco install git -y
choco install keepassxc -y
choco install nordvpn -y
choco install obs-studio -y
choco install python3 -y
choco install adoptopenjdk -y
choco install rust -y
choco install spotify -y
choco install steam -y
choco install steelseries-engine -y
choco install vscode -y
choco install openoffice -y
choco install veracrypt -y
choco install auto-dark-mode -y
choco install powertoys -y